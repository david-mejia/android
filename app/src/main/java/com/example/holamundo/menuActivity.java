package com.example.holamundo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class menuActivity extends AppCompatActivity {

    private CardView crvHola,crvImc,crvMoneda,crvCotizacion,crvConversion,crvSpinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_menu);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        initComponents();

        crvHola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        crvImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, IMC_Activity.class);
                startActivity(intent);
            }
        });

        crvMoneda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, MonedaActivity.class);
                startActivity(intent);
            }
        });

        crvConversion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, ConversionActivity.class);
                startActivity(intent);
            }
        });

    }

    private void initComponents(){
        crvConversion = (CardView) findViewById(R.id.crvConversion);
        crvHola = (CardView) findViewById(R.id.crvHola);
        crvImc = (CardView) findViewById(R.id.crvImc);
        crvMoneda = (CardView) findViewById(R.id.crvMoneda);
        crvSpinner = (CardView) findViewById(R.id.crvSpinner);
        crvCotizacion = (CardView) findViewById(R.id.crvCotizacion);
    }
}